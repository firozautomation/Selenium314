package claseUno;

import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;

public class GoolgleSearch {

	public static void main(String[] args) {

		WebDriver driver;
		WebElement element;
		
		String driverPath = "C://Selenium//chromedriver.exe";
		System.setProperty("webdriver.chrome.driver", driverPath);
	
		driver = new ChromeDriver();
	
		driver.manage().window().maximize();
		
		
		// Navigate
		try {
			
			driver.navigate().to("https://www.google.com");
		
		}catch(Exception ex) {
			System.out.println(ex.getMessage());
		}
		// Get
		//driver.get("https://www.google.com");
		
		// Buscar Hexaware
		element = driver.findElement(By.name("q"));
		element.sendKeys("Hexaware");
		element.sendKeys(Keys.ENTER);
		
		element = driver.findElement(By.partialLinkText("Hexaware - IT, BPO,"));
		element.click();		
		driver.quit();
	}	
	
}
