package claseUno;

import java.util.List;
import java.util.concurrent.TimeUnit;

import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

public class YoutubeSearch {

	public static void main(String[] args) throws InterruptedException {

		WebDriver driver;
		WebElement element = null;
		WebDriverWait varWait;

		String driverPath = "C://Selenium//chromedriver.exe";
		System.setProperty("webdriver.chrome.driver", driverPath);

		driver  = new ChromeDriver();
		varWait = new WebDriverWait(driver, 10);
		driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);


		try {

			driver.navigate().to("https://www.youtube.com/");

		}catch(Exception ex) {
			System.out.println(ex.getMessage());
		}

		element = driver.findElement(By.id("search"));
		element.sendKeys("Hexaware");
		element.sendKeys(Keys.ENTER);


		List<WebElement> elements = driver.findElements(By.cssSelector("#contents ytd-video-renderer"));

		elements.forEach((x) -> {

			int index = elements.indexOf(x) + 1;
			WebElement e  = driver.findElement(By.cssSelector("#contents ytd-video-renderer:nth-child(" + index + ")"));
			e.click();

			WebElement title = driver.findElement(By.cssSelector("#container h1"));
			varWait.until(ExpectedConditions.visibilityOf(title));
			System.out.println(String.format("Video %d: %s", index, title.getText()));


			try {
				Thread.sleep(10000);
			} catch (InterruptedException e1) {
				e1.printStackTrace();
			}


			driver.navigate().back();

		});


		/*for(int i = 1; i <= elements.size(); i++) {

			element = driver.findElement(By.cssSelector("#contents ytd-video-renderer:nth-child(" + i  + ")"));
			element.click();

			Wait(5);

			WebElement title = driver.findElement(By.cssSelector("#container h1"));
			System.out.println(String.format("Video %d: %s", i, title.getText()));

			Wait(10);			
			driver.navigate().back();

		}*/		
	}


	public static void Wait(int s) throws InterruptedException {
		Thread.sleep(s* 1000);

	}
}
