package claseUno;

import java.util.Set;

import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.Select;
import org.openqa.selenium.support.ui.WebDriverWait;

public class TrivagoTest {

	static WebDriver driver;
	
	public static void main(String[] args) throws InterruptedException {

		
		
		WebElement element;
		Select s;
		JavascriptExecutor js;
		
		String driverPath = "C://Selenium//chromedriver.exe";
		System.setProperty("webdriver.chrome.driver", driverPath);
	
		driver = new ChromeDriver();
	
		driver.manage().window().maximize();
		js = ((JavascriptExecutor)driver);
		
		
		WebDriverWait varWait = new WebDriverWait(driver, 10);
		
		
		try {
			
			driver.navigate().to("https://www.trivago.com");
			
		}catch(Exception ex) {
			System.out.println(ex.getMessage());
		}
		
		
		// Search for Vancouver
		element = driver.findElement(By.id("horus-querytext"));
		element.sendKeys("Vancouver");
		element.sendKeys(Keys.ENTER);
		
		Wait(10);
		
		// check if calendar is displayed
		element = driver.findElement(By.className("df_overlay"));
		if(element.isDisplayed())
			varWait.until(ExpectedConditions
					.elementToBeClickable(
							driver.findElement(By.xpath("//*[@datetime=\"2018-09-25\"]"))
							)).click();
		else
			System.out.println("Calendar is not present");
		
		// Click in Checkout calendar
		element = driver.findElement(By.xpath("//*[@datetime=\"2018-09-27\"]"));
		varWait.until(ExpectedConditions.elementToBeClickable(element)).click();
		
		Wait(5);
		// chose single room
		element = driver.findElement(By.xpath("(//*[@role=\"menuitem\"])[1]"));
		varWait.until(ExpectedConditions.elementToBeClickable(element)).click();
		
		Wait(10);
		
		if(driver.getCurrentUrl().contains(".mx"))
			element = driver.findElement(By.xpath("//*[text()=\"Ok\"]"));
		else
		element = driver.findElement(By.xpath("//*[text()=\"Done\"]"));
		
		
		if(element.isDisplayed())
			varWait.until(ExpectedConditions.elementToBeClickable(element)).click();
		
		Wait(10);
		((JavascriptExecutor)driver).executeScript("window.scrollBy(0, 100);");
		// click in view deal
		element = driver.findElement(By.xpath("(//*[@class=\"deal__wrapper\"])[1]"));
		varWait.until(ExpectedConditions.elementToBeClickable(element)).click();
		
		
		varWait.until(ExpectedConditions.numberOfWindowsToBe(2));
		SwitchToWindows();
		
		((JavascriptExecutor)driver).executeScript("window.scrollBy(0, 500);");
		
		/*element = driver.findElement(By.xpath("(//*[@class=\"roomFooter\"]//a)[1]"));
		if(element.isDisplayed())
			element.click();
		*/
		
		
	}
	
	public static void SwitchToWindows() {
		
		String currWin = driver.getWindowHandle();
		Set<String> windows = driver.getWindowHandles();
		windows.remove(currWin);
		
		for(String x : windows) {
			
			if(x != currWin) {
				driver.switchTo().window(x);
				System.out.println(String.format("CurrWin: %s , SwitchToWindows: %s", currWin, x));
			}
			
		}
		
	}
	
	public static void Wait(int s) throws InterruptedException {
		Thread.sleep( s * 1000);
	}
	

	
}
